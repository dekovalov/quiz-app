import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:provider/provider.dart';
import 'package:quiz/database/firestore_service.dart';
import 'package:quiz/model/question_model.dart';
import 'package:quiz/score_provider.dart';
import 'package:quiz/screens/finish_screen.dart';
import 'package:quiz/screens/quiz_screen.dart';
import 'package:quiz/screens/start_page.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group('End to end', () {
    testWidgets('app test', (tester) async {
      // Populate the fake database.
      final fakeFirestore = FakeFirebaseFirestore();


        await fakeFirestore.collection('questions').add({
          'title': 'What is 2+2?',
          'options': {'4': true, '3': false, '4,5': false, '5': false},
          'id': 1,
        });

      await fakeFirestore.collection('questions').add({
        'title': 'What is 3+3?',
        'options': {'5': false, '6': true, '7': false, '8': false},
        'id': 2,
      });

      await fakeFirestore.collection('questions').add({
        'title': 'What is 4+4?',
        'options': {'7': true, '9': false, '8': false, '10': false},
        'id': 3,
      });


      // Build the app
      await tester.pumpWidget(MaterialApp(
        title: 'Example',
        home: StartPage(firestore: fakeFirestore),
      ));

      await tester.pumpAndSettle();

      // Find the LETS START button
      final startButtonFinder = find.byKey(const Key('start'));
      // Check for a button on the screen
      expect(startButtonFinder, findsOneWidget);

      // Tap the button
      await tester.tap(startButtonFinder);
      await tester.pumpAndSettle();
      // Check if we are on the QuizPage
      expect(find.byType(QuizPage), findsOneWidget);

      final List<QuestionModel> question = await FirestoreService(fakeFirestore).getAllQuestions();
      for (var i = 0; i < question.length; i++) {
        final currentQuestion = question[i];

        // Checking the display of the QuestionTile.
        expect(find.text(currentQuestion.title), findsOneWidget);

        // Taping the 'True' answer
        await tester.tap(find.byKey(Key('answer$i')));
        await tester.pumpAndSettle();
        final scoreProvider = Provider.of<ScoreProvider>(tester.element(find.byType(QuizPage)), listen: false);

        // Checking that the score has been updated.
        if (i <= 1) {
          expect(scoreProvider.score, i + 1);
        } else {
          expect(scoreProvider.score, 2);
        }

        // Go to the next question
        if (i < question.length) {
          await tester.tap(find.byKey(const Key('next')));
          await tester.pumpAndSettle();
        }
      }

      expect(find.byType(FinishScreen), findsOneWidget);

      // Reset score
      await tester.tap(find.byKey(const Key('reset')));
      await tester.pumpAndSettle();

      expect(find.byType(StartPage), findsOneWidget);
      expect(ScoreProvider().score, 0);
    });
  });
}
