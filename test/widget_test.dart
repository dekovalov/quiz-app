import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:provider/provider.dart';
import 'package:quiz/components/answer_tile.dart';
import 'package:quiz/model/question_model.dart';
import 'package:quiz/score_provider.dart';
import 'package:quiz/screens/quiz_screen.dart';

void main() {
  // Create a mock QuestionModel for testing
  final QuestionModel mockQuestion = QuestionModel(
    title: 'What is 2 + 2?',
    options: {
      '4': true,
      '5': false,
      '6': false,
      '7': false,
    },
    id: 1,
  );

  testWidgets('QuizPage displays correctly and AnswerTiles work', (WidgetTester tester) async {
    // Build our QuizPage widget with the necessary providers
    await tester.pumpWidget(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (context) => ScoreProvider(),
          ),
        ],
        child: MaterialApp(
          home: QuizPage(
            question: mockQuestion,
            questionsLength: 1,
            currentPage: 0,
            pageController: PageController(),
          ),
        ),
      ),
    );

    // Verify that the QuizPage displays the question
    expect(find.text('What is 2 + 2?'), findsOneWidget);

    // Tap an answer option (for example, option 'A')
    await tester.tap(find.text('5'));
    await tester.pumpAndSettle();



    // Verify that the AnswerTile border colors change according to the selected answers
    final answerTile0 = find.byKey(const Key('answer0')).evaluate().first.widget as AnswerTile;
    final answerTile1 = find.byKey(const Key('answer1')).evaluate().first.widget as AnswerTile;
    final answerTile2 = find.byKey(const Key('answer1')).evaluate().first.widget as AnswerTile;
    final answerTile3 = find.byKey(const Key('answer1')).evaluate().first.widget as AnswerTile;

    if (answerTile0.selectedAnswerIndex == 0) {
      expect(answerTile0.answerColor, Colors.green);
      expect(answerTile1.answerColor, Colors.red);
      expect(answerTile2.answerColor, Colors.white);
      expect(answerTile3.answerColor, Colors.white);
    }
  });
}
