import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:quiz/model/question_model.dart';
import 'package:quiz/validator/validator.dart';

class FirestoreService {
  final FirebaseFirestore _firestore;

  FirestoreService(this._firestore);

  Future<List<QuestionModel>> getAllQuestions() async {
    QuerySnapshot snapshot = await _firestore.collection("questions").
    orderBy("id", descending: false).get();

    List<QuestionModel> questions = [];

    for (var doc in snapshot.docs) {
      try {
        QuestionModel question = QuestionModel.fromJson(doc.data() as Map<String, dynamic>);
        if (Validator.isValidQuestion(question, questions)) {
          questions.add(question);
        }
      } catch (e) {
        debugPrint("Error: $e");
      }
    }

    return questions;
  }
}