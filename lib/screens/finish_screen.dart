import 'package:flutter/material.dart';
import 'package:quiz/constants/constants.dart';
import 'package:quiz/score_provider.dart';

class FinishScreen extends StatefulWidget {
  final ScoreProvider scoreProvider;

  const FinishScreen({super.key, required this.scoreProvider});

  @override
  State<FinishScreen> createState() => _FinishScreenState();
}

class _FinishScreenState extends State<FinishScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset('assets/winner.png'),
              Text(
                'Your Score: ${widget.scoreProvider.score}',
                style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
              const Text(
                'Everyone is a winner',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 30),
              TextButton(
                  style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    backgroundColor: ColorSelect.primaryGreen,
                    minimumSize: Size(MediaQuery.of(context).size.width * 0.7, 60),
                  ),
                  key: const Key('reset'),
                  onPressed: () {
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    widget.scoreProvider.resetScore();
                  },
                  child: const Text(
                    "Reset Game",
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
