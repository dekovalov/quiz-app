import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:quiz/constants/constants.dart';
import 'package:quiz/database/firestore_service.dart';
import 'package:quiz/model/question_model.dart';
import 'package:quiz/score_provider.dart';
import 'package:quiz/screens/quiz_screen.dart';

class StartPage extends StatefulWidget {
  final FirebaseFirestore firestore;

  const StartPage({super.key, required this.firestore});

  @override
  State<StartPage> createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> {
  final PageController pageController = PageController();

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  Future<List<QuestionModel>>? _loadData() async {
    FirestoreService firestoreService = FirestoreService(widget.firestore);
    return await firestoreService.getAllQuestions();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: [
            Center(
              child: FutureBuilder<List<QuestionModel>>(
                future: _loadData(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                        child: CircularProgressIndicator(
                      color: ColorSelect.primaryGreen,
                    ));
                  } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text('Data not found', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                          const SizedBox(height: 30),
                          TextButton(
                            style: TextButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              backgroundColor: ColorSelect.primaryGreen,
                              minimumSize: Size(MediaQuery.of(context).size.width * 0.7, 60),
                            ),
                            onPressed: () {
                              setState(() {
                                _loadData();
                              });
                            },
                            child: const Text('Try again', style: TextStyle(color: Colors.white)),
                          ),
                        ],
                      ),
                    );
                  } else {
                    List<QuestionModel> questions = snapshot.data!;
                    return Column(
                      children: [
                        const SizedBox(height: 30),
                        Expanded(
                          flex: 3,
                          child: Image.asset('assets/developer.png'),
                        ),
                        const Text('Quiz for developers!', style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
                        const Padding(
                          padding: EdgeInsets.all(10),
                          child: Text("Let's improve your knowledge", style: TextStyle(fontSize: 18)),
                        ),
                        const SizedBox(height: 30),
                        TextButton(
                          key: const Key('start'),
                          style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            backgroundColor: ColorSelect.primaryGreen,
                            minimumSize: Size(MediaQuery.of(context).size.width * 0.7, 60),
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) {
                                int pageCount = questions.length;
                                List<Widget> quizPages = List.generate(
                                  pageCount,
                                  (index) => QuizPage(
                                    question: questions[index],
                                    questionsLength: questions.length,
                                    currentPage: index,
                                    pageController: pageController,
                                  ),
                                );

                                return ChangeNotifierProvider(
                                  create: (context) => ScoreProvider(),
                                  child: PageView(
                                    physics: const NeverScrollableScrollPhysics(),
                                    controller: pageController,
                                    children: quizPages,
                                  ),
                                );
                              }),
                            );
                          },
                          child: const Padding(
                            padding: EdgeInsets.all(16.0),
                            child: Text(
                              'LETS START',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                        const SizedBox(height: 50),
                      ],
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
