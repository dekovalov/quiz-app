import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:quiz/components/answer_tile.dart';
import 'package:quiz/components/bottom_button.dart';
import 'package:quiz/components/question_tile.dart';
import 'package:quiz/constants/constants.dart';
import 'package:quiz/model/question_model.dart';
import 'package:quiz/score_provider.dart';
import 'package:quiz/screens/finish_screen.dart';

class QuizPage extends StatefulWidget {
  final QuestionModel question;
  final int questionsLength;
  final int currentPage;
  final PageController pageController;

  const QuizPage(
      {super.key,
      required this.question,
      required this.questionsLength,
      required this.currentPage,
      required this.pageController});

  @override
  State<QuizPage> createState() => _QuizPageState();
}

class _QuizPageState extends State<QuizPage> {
  bool _isSelected = false;
  int _selectedAnswerIndex = -1;

  @override
  Widget build(BuildContext context) {
    bool isQuizFinished = widget.currentPage + 1 == widget.questionsLength ? true : false;
    var optionText = widget.question.options.keys.toList();
    var optionValues = widget.question.options.values.toList();

    final scoreProvider = Provider.of<ScoreProvider>(context);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorSelect.background,
        automaticallyImplyLeading: false,
        title: Text(
          '${widget.currentPage + 1} / ${widget.questionsLength}',
          style: const TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
        ),
        centerTitle: true,
      ),
      backgroundColor: ColorSelect.background,
      body: SafeArea(
        child: Column(
          children: [
            const SizedBox(height: 30),
            QuestionTile(question: widget.question.title),
            const Spacer(),
            ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: widget.question.options.length,
                itemBuilder: (context, index) {
                  return AnswerTile(
                    key: Key("answer$index"),
                    option: optionValues[index],
                    selectedAnswer: () => selectedAnswer(index, optionValues, scoreProvider),
                    answerText: optionText[index],
                    answerColor: _isSelected
                        ? optionValues[index]
                            ? Colors.green
                            : Colors.white
                        : null,
                    selectedAnswerIndex: _selectedAnswerIndex,
                  );
                }),
            const Spacer(),
            BottomButton(
              key: const Key('next'),
              title: isQuizFinished ? 'Finish' : null,
              nextPageCallback: () {
                if (_isSelected) {
                  isQuizFinished
                      ? Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => FinishScreen(scoreProvider: scoreProvider)),
                        )
                      : widget.pageController.nextPage(
                          duration: const Duration(milliseconds: 300),
                          curve: Curves.easeIn,
                        );
                } else {
                  return;
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  void selectedAnswer(int index, List<bool> optionValues, ScoreProvider scoreProvider) {
    if (_selectedAnswerIndex == -1) {
      if (optionValues[index] == true) {
        scoreProvider.incrementScore();
      }

      setState(() {
        _isSelected = true;
        _selectedAnswerIndex = index;
      });
    }
    _selectedAnswerIndex = index;
  }
}
