
class QuestionModel {
  final int id;
  final String title;

  final Map<String, bool> options;

  QuestionModel({required this.id, required this.title, required this.options});

  @override
  String toString() {
    return 'QuestionModel{id: $id, title: $title, options: $options}';
  }


  factory QuestionModel.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> optionsJson = json['options'];
    Map<String, bool> options = {};

    for (var entry in optionsJson.entries) {
      options[entry.key] = entry.value as bool;
    }

    return QuestionModel(
      id: json['id'],
      title: json['title'],
      options: options,
    );
  }
}