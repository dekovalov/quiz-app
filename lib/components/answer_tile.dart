import 'package:flutter/material.dart';

class AnswerTile extends StatefulWidget {
  final bool option;
  final VoidCallback selectedAnswer;
  final String answerText;
  final Color? answerColor;
  final int selectedAnswerIndex;

  const AnswerTile(
      {super.key,
      required this.option,
      required this.selectedAnswer,
      required this.answerText,
      this.answerColor,
      required this.selectedAnswerIndex});

  @override
  State<AnswerTile> createState() => _AnswerTileState();
}

class _AnswerTileState extends State<AnswerTile> {
  bool _isSelected = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: GestureDetector(
        onTap: () {
          widget.selectedAnswer();
          if (widget.selectedAnswerIndex == -1) {
            setState(() {
              _isSelected = true;
            });
          }
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
            border: Border.all(
                color: _isSelected
                    ? widget.option == true
                        ? Colors.green
                        : Colors.red
                    : widget.answerColor ?? Colors.white),
          ),
          margin: const EdgeInsets.only(left: 24, right: 24),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 24),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  widget.answerText,
                  style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
