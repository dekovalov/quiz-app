import 'package:flutter/material.dart';
import 'package:quiz/constants/constants.dart';


class BottomButton extends StatelessWidget {
  final VoidCallback nextPageCallback;
  final String? title;
  const BottomButton({Key? key, required this.nextPageCallback, this.title}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: nextPageCallback,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(24, 0, 24, 20),
        child: Container(
          height: 60,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: ColorSelect.primaryGreen,
          ),
          child:  Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Center(
              child: Text(
                title == null ? 'Next' : 'Finish',
                style: const TextStyle(fontSize: 24, fontWeight: FontWeight.w500, color: Colors.white),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
