import 'package:flutter/material.dart';

class QuestionTile extends StatefulWidget {
  final String question;

  const QuestionTile({super.key, required this.question});

  @override
  State<QuestionTile> createState() => _QuestionTileState();
}

class _QuestionTileState extends State<QuestionTile> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 5,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.15),
              blurRadius: 50.0,
              offset: const Offset(0, 20),
              spreadRadius: -10,
            )
          ],
        ),
        margin: const EdgeInsets.only(left: 24, right: 24),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 24),
            child: SingleChildScrollView(
              child: Text(
                textAlign: TextAlign.center,
                widget.question,
                style: const TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
