import 'package:flutter/material.dart';
import 'package:quiz/model/question_model.dart';

class Validator {
  static bool isValidQuestion(QuestionModel? question, List<QuestionModel> correctQuestions) {
    // If question empty
    if (question == null) {
      debugPrint("Question is null");
      return false;
    }
    // If id is a duplicate
    if (correctQuestions.any((existingQuestion) => existingQuestion.id == question.id)) {
      debugPrint("Id is a duplicate");
      return false;
    }
    // If title is a duplicate
    if (correctQuestions.any((existingQuestion) => existingQuestion.title == question.title)) {
      debugPrint("Title is a duplicate");
      return false;
    }
    // If id is empty
    if (question.id.isNegative) {
      debugPrint("Question id is negative");
      return false;
    }
    // If title is empty
    if (question.title.isEmpty) {
      debugPrint("Invalid title: ${question.id}");
      return false;
    }
    // Is answers is empty or != 4
    if (question.options.isEmpty || question.options.length != 4) {
      debugPrint("Invalid options: ${question.id}");

      return false;
    }
    // Checking the correctness of answers, there must be one
    int correctAnswerCount = 0;
    question.options.forEach((option, isCorrect) {
      if (isCorrect) {
        correctAnswerCount++;
      }
    });

    if (correctAnswerCount != 1) {
      debugPrint("Many correct answers: ${question.id}");
      return false;
    }

    return true;
  }
}
