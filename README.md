# Quiz for developers - Grow!
---
"Quiz for developers - Grow!" App using Flutter🌟
Designed for practice integration and widget tests

You can [download] the latest version of Todo for Android.\
[<img src="https://github.com/weluid/Todo-App/assets/124319560/68097b68-3540-42d4-85dc-ead0e4c7e404" width ="300"/>](https://play.google.com/store/apps/details?id=com.dekovalov.quiz)

## Tech
---
Libraries used in the project:

- [Provider] - state management solution
- [Cloud Firestore] - a Flutter plugin to use the Cloud Firestore API
- [Fake Cloud Firestore] - fakes to write unit tests for Cloud Firestore
- [Firebase Crashlytics] - realtime crash reporter that helps you track, prioritize, and fix errors
- [Firebase Core] - connecting app to Firebase


[Provider]: <https://pub.dev/packages/provider>
[Fake Cloud Firestore]: <https://pub.dev/packages/fake_cloud_firestore>
[Cloud Firestore]: <https://pub.dev/packages/cloud_firestore>
[Firebase Core]: <https://pub.dev/packages/firebase_core>
[Firebase Crashlytics]: <https://pub.dev/packages/firebase_crashlytics>
[download]: <https://play.google.com/store/apps/details?id=com.dekovalov.quiz>

![gif](https://github.com/weluid/Todo-App/assets/124319560/700a9f73-8b10-4b97-8a9d-f58a58b29c76)
